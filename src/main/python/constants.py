from configparser import SafeConfigParser
import copy
import os

cfg = SafeConfigParser()
if not os.path.exists("config.cfg"):
	cfg.read("default.cfg")	
else:
	cfg.read("config.cfg")