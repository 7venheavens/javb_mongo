import os
import re
import requests
import logging
from multiprocessing.pool import ThreadPool
from collections import defaultdict
import constants

from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
import errors

from searcher import Searcher
from data_structures.trie import Trie

logging.basicConfig(filename='data/log.log',level=logging.DEBUG)


class Model:
    def __init__(self, database = "", lang = "en", debug = 0):
        # Initialize configuration
        self.cfg = constants.cfg
        if debug:
            for key, item in self.cfg["app_debug"].items():
                self.cfg["app"][key] = item
            

        self.lang = self.cfg["app"]["JAVB_lang"]
        if database :
            self.database = database
        else: 
            self.database = self.cfg["app"]["db_name"]
        print("Using database ", self.database)
        self.covers_dir = "data/covers/" + self.database

        # Initialize mongodb database
        try:
            self.client = MongoClient()
            self.db = self.client[self.database]
            self.vcol = self.db["videos"]
            self.vcol.ensure_index("path")
            # Initialize video and tag search caches for faster searches
            # Videos has the following structure: {code,  path, _id, en{stars, title, etc}, jp{stars, title,}}}
            
        except ServerSelectionTimeoutError as err:
            print("Unable to find/create mongodb instance. Is mongodb started?")

        self.init_cache()

        # Initialize JAV searcher (Various, but only javlibrary implemented)
        self.searcher = Searcher()

        

    def reset_database(self):
        print(self.db)
        print(self.database)
        self.client.drop_database(self.database)
        self.__init__(self.database, self.lang)
        return 0

    def init_cache(self):
        """
        Reacquires data from mongodb and rebuilds all caches needed for JAVB
        """
        self.videos = {i["path"]: i for i in self.vcol.find()}
        self.tags = {
            "en": defaultdict(set),
            "jp": defaultdict(set),
            "all": defaultdict(set),}
        for key, video_entry in self.videos.items():
            self.update_tags(video_entry)   
        self.all_paths = {i for i in self.videos}
        
        self.tag_aliases = self.cfg["tag_aliases"]
        self.code_trie = Trie()
        # print("CODES:", [item["code"] for key, item in self.videos.items()])
        codes, items = [], []
        for key, item in self.videos.items():
            codes.append(item["code"].lower())
            items.append(item)
        self.code_trie.add_many(codes, items)
        print(self.code_trie.top)

    def get_statistics(self):
        return {
            "video_count": len(self.videos),
            "video_count_db": self.vcol.count()
         }

    def update_tags(self, video_entry):
        """
            Given a video_entry, adds it's information to the tag cache without saving to the database
        """
        # print("Processing {}".format(video_entry["code"]))
        for language in ["en", "jp"]:
            for category in video_entry[language]:
                # print("processing category: ", category)
                if category == "genre":
                    for tag in video_entry[language][category]:
                        self.tags[language][tag.lower()].add(video_entry["path"])
                        self.tags["all"][tag.lower()].add(video_entry["path"])
                elif category == "star": #handle the cases where the category can have multiple entries
                    for tag in video_entry[language][category]:
                        self.tags[language][category + ":" + tag.lower()].add(video_entry["path"])
                        self.tags["all"][category + ":" + tag.lower()].add(video_entry["path"])
                else: #Handle the case where the category has one entry (Code et al.)
                    if not video_entry[language][category]:
                        # print("entry contains no data")
                        continue
                    self.tags[language][(category + ":" + video_entry[language][category]).lower()].add(video_entry["path"])
                    self.tags["all"][(category + ":" + video_entry[language][category]).lower()].add(video_entry["path"])
        return 0

    def get_all_tags(self):
        """
        returns a list of all tags and their respective categoriess
        """
        return [i.replace(" ", "_") for i in self.tags[self.lang]]
            
    def tag_videos(self, dir_path, retag = 0, threads = 0):
        """
        Integration script. Given a directory, tags all videos using information from javlibrary
        stores result. Currently not used
        Args:
            dir_path: <str> path to directory containing videos to be tagged
            debug   : <bool> Debug mode
            retag   : <bool> Retag all videos (skips existing entries if false)
        Returns:
        """
        if not threads:
            threads = int(self.cfg["app"]["threadcount"])
        video_paths = get_video_paths(dir_path)

        # Remove tagged paths from list unless retagging
        if not retag:
            unique_paths = []
            for video_path in video_paths:
                if video_path in self.all_paths:
                    continue
                else:
                    unique_paths.append(video_path)
            video_paths = unique_paths
                    
        linked, unlinked = link_code_to_videos(video_paths)
        print("TAG LINKED VIDEOS THEADS: ", threads)
        self.tag_linked_videos(threads = threads)
        return (linked, unlinked,)

    def tag_linked_videos(self, linked_videos, debug =0, threads = 0, chunksize = 50):
        """
        For every video code provided, obtains tags from online database and stores 
        them in both JAVB active memory and mongodb database
        for tracking purposes, it behaves as an iterator, spitting out the total and current number processed
        """
        #define the holders, they're here to make tag_videos easier
        multi_hit, no_hit = [], []

        def tag_video(code, path):
            try:
                print("Tagging {}".format(code))
                data, data_jp = self.searcher.get_details(code)
            except errors.SearchNoResults:
                logging.info("SearchNoResults: " + path)
                no_hit.append((code, path))
                return
            except errors.SearchMultipleResults:
                logging.info("SearchMultipleResults: " + path)
                multi_hit.append((code, path))
                return 
            except Exception as e:
                raise

            if debug:
                print("adding item {} with path {}".format(code, path))

            # catch null results from searcher
            if data["code"] != code:
                print("Skipping , data['code'] {} != {}".format(data["code"], code))
                no_hit.append((code, path))


            # update videos, tags, and paths
            video_entry = {"path": path, "code": code, "en": data, "jp": data_jp, "saved": 0} #tag as unsaved for later updates
            self.videos[path] = video_entry
            self.update_tags(video_entry)
            self.all_paths.add(path)
        if not threads:
            threads = int(self.cfg["app"]["threadcount"])

        try:
            assert chunksize >= threads
        except:
            print("Please set a chunksize greater than the threadcount")
            raise 
        
        print("Tagging videos with {} threads".format(threads))
        pool = ThreadPool(threads)

        video_count = len(linked_videos)
        chunks = [linked_videos[i: i+chunksize] for i in range(0, video_count, chunksize)]
        # Return current tag state
        yield (0, video_count, 0, 0)
        for num, chunk in enumerate(chunks):
            pool.starmap(tag_video, chunk)
            # Current processed, total, failure count
            yielded = (min((num+1) *50, video_count), video_count, len(no_hit), len(multi_hit))
            print(yielded)
            yield yielded

        #save new stuff in database
        for path, entry in self.videos.items():
            if "saved" in entry and entry["saved"] == 0:
                del entry["saved"]
                self.vcol.update({"path": path}, entry, upsert = True)
        with open("data/sublogs/no_hit.txt", "w", encoding = "utf-8") as f:
            for code, path in no_hit:
                f.write("{}\t{}\n".format(code, path))
        with open("data/sublogs/multi_hit.txt", "w", encoding = "utf-8") as f:
            for code, path in no_hit:
                f.write("{}\t{}\n".format(code, path))

    def search(self, string, and_flag = False, debug = False):
        """
        Searches the database for entries matching a specific tag. String is processed
        and tag-type data acquired and used to search
        Sample tag queries:
            anal            
            star:Ichigo_aoi 
        returns 
        """
        terms = [i.replace("_", " ") for i in string.lower().split() if i != ""]
        print("SEARCHING: ", terms)
        if len(terms) == 1 and terms[0].startswith("code:") and terms[0][-1] != ":":
            term = terms[0][5:]
            return self.get_videos_startswith(term)

        # Acquire sets for each search term
        raw = [self.tags[self.lang][term] for term in terms] 
        raw = [i for i in raw if i]
        print("raw: ", raw)
        if len(raw) == 0:
            print("no result")
            return raw

        base = raw[0]
        if debug:
            print("BASE: ", base)
        if and_flag:
            for datum in raw[1:]:
                base = base.intersection(datum)
        else:
            for datum in raw[1:]:
                if debug:
                    print("UNION WITH:", datum)
                base = base.union(datum)
        res = sorted([self.videos[i] for i in base], key = lambda x: x["code"])
        # print(self.videos)
        # print(res)
        return res

    def get_videos_startswith(self, code):
        return self.code_trie.search(code)




def is_video(file_path):
    file_name = os.path.split(file_path)[1]
    file_ext = os.path.splitext(file_name)[1][1:]

    if file_ext.lower() in {'3gp', 'aac', 'asf', 'avi', 'dvr-ms', 'flc', 'fli', 'flv', 'iso', 
                            'm4a', 'm4v', 'm4p', 'mp2', 'mka', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'nsa', 
                            'nut', 'nuv', 'ogg', 'ogm', 'pva', 'qt', 'ra', 'ram', 'rm', 
                            'rmvb', 'vivo', 'vob', "webm", 'wmv', 'wtv'}:
        return True
    return False

def get_dirs_without_video(dir_path):
    # purge trailing slashes
    dir_path = dir_path.rstrip("\\")
    dir_path = dir_path.rstrip("/")
    res = []
    for path, dirs, files in os.walk(dir_path):
        if os.path.split(path)[0] != dir_path:
            # print(os.path.split(path)[0], dir_path)
            continue
        if not any(is_video(file) for file in files):
            res.append(path)
    return res


def get_video_paths(dir_path):
    """
    Collect all video files within a directory
    """
    # purge trailing slashes
    dir_path = dir_path.rstrip("\\")
    dir_path = dir_path.rstrip("/")
    res = []
    for path, dirs, files in os.walk(dir_path):
        for file in files:
            if is_video(file):
                res.append(os.path.abspath(os.path.join(path, file)))
    return res

def link_code_to_videos(video_paths):
    """
    links video codes for all videos provided by get_video_paths
    input: list of strings. Each string is a path to a video file
    returns: tuple containing two lists (linked, unlinked). 
             Linked: [key, path] for each file with a found video code
             unlinked: path for each file with no found video code
    """
    title_regex = re.compile(r"""([a-zA-Z]+)[-_]?([0-9]+)""")
    linked = []
    unlinked = []
    # account for files with known false positive codes
    false_flags = {("SIS", "001"), ("ses", "23")}
    for path in video_paths:
        candidate = title_regex.search(path)
        if not candidate:
            unlinked.append(path)
            continue
        candidate = candidate.groups()
        # filter out known false flags
        if candidate in false_flags:
            candidate = title_regex.findall(path)[-1]
        
        key = "-".join(candidate).upper()
        linked.append((key, path))
    return (linked, unlinked)

if __name__ == "__main__":
    print("Testing basic file processing")

    video_paths = get_video_paths("data\\test_data")
    # print(video_paths)
    # print(len(video_paths))
    non_video_paths = get_dirs_without_video("data\\test_data\\")
    # print(non_video_paths)
    # print(len(non_video_paths))
    print("Processing test data set:")
    print("{} video paths found, {} non-video paths found".format(len(video_paths), len(non_video_paths)))

    linked_videos, unlinked = link_code_to_videos(video_paths)
    print("Testing linkage of video codes to video paths")
    testcases = ["KTDS-871", "HELL-102", "SNIS-684", "MIGD-723"]
    linked_codes ={i[0] for i in linked_videos}
    for testcase in testcases:
        print("Testing: {}".format(testcase))
        assert testcase in linked_codes

    test_data = linked_videos[:5]

    print("Testing JAVB Model")
    m = Model(database = "JAVB_TEST")
    m.reset_database()
    
    print("Tagging linked videos")
    for cur, total, failed, multi in  m.tag_linked_videos(test_data, debug = 1, threads = 4):
        print("Processed {} / {}, failed: {}, multi: {}".format(cur, total, failed, multi))
    print("Checking tags")
    print([i for i in m.tags["en"]])
    print("Checking search")
    print(m.search("star:atomi_shuri 3p", and_flag = False, debug = 1))
    print("All tags")
    print(m.get_all_tags())
    m.init_cache()
    print(m.search("code:hell"))