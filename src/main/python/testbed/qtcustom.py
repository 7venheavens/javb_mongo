from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt


class Completer(QtWidgets.QCompleter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCaseSensitivity(Qt.CaseInsensitive)
    def pathFromIndex(self, index):
        path = QtWidgets.QCompleter.pathFromIndex(self, index)
        lst = str(self.widget().text()).split(' ')

        if len(lst) > 1:
            path = "{} {}".format(' '.join(lst[:-1]), path)
        return path

    # Add operator to separate between texts
    def splitPath(self, path):
        path = str(path.split(' ')[-1]).lstrip(' ')
        return [path]