from PyQt5.QtGui import * 
from PyQt5.QtCore import * 
from PyQt5.QtWidgets import *
import sys

from qtcustom import Completer



def main():    
    app     = QApplication(sys.argv)
    edit    = QLineEdit()
    strList = ["Germany", "Spain", "France", "Norway"]
    completer = Completer(strList,edit)

    edit.setCompleter(completer)
    edit.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()