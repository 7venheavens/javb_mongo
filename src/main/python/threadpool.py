from threading import Thread
from queue import Queue

def Worker(Thread):
	def __init__(self, tasks):
		super().__init__()
		self.tasks = tasks
		self.daemon = True
		self.start()

	def run(self):
		while True:
			func, args, kwargs = self.tasks.get()
			try:
				func(*args, **kwargs)
			except Exception as e:
				print(e)
			finally:
				self.tasks.task_done()

def ThreadPool:
	def __init__(self, threadcount = 1):
		self.tasks = Queue(threadcount)
		for i in range(threadcount):
			Worker(self.tasks)

	def add_task(self, func, *args, **kwargs):
		self.tasks.put(func, args, kwargs)

	def wait_completion(self):
		self.tasks.join()

def 






if __name__ == "__main__":
	pass
