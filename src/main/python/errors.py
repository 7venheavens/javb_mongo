
class SearchNoResults(Exception):
	pass

class SearchMultipleResults(Exception):
	pass

class FailedToAcquireTags(Exception):
	pass


