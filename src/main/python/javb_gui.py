#!/usr/bin/env python3

import sys
import subprocess
import os
import threading
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QMessageBox, QLabel
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt, pyqtSignal

import javb_core as core
from utilities.qtcustom import Completer
import errors
# implement mongodb check

class Main(QMainWindow):
	# Signals must be at class level
	update_statusbar_trigger = pyqtSignal(str)

	def __init__(self, debug = 0):
		super().__init__()
		self.model = core.Model(debug = debug)
		self.loaded = dict()
		self.lang = "en"
		self.initUI()
		self.init_triggers()
		self.update_status_bar("ready")

	def init_menus(self):
		menubar = self.menuBar()

		# File menu
		## Load folder
		load_folder = QtWidgets.QAction(QtGui.QIcon("open.png"), "&Load folder", self)
		load_folder.setShortcut("Ctrl+L")
		load_folder.setStatusTip("Load target directory into JAVB")
		load_folder.triggered.connect(self.load_folder)

		save_action = QtWidgets.QAction(QtGui.QIcon("save.png"), "&Save edits", self)
		save_action.setShortcut("Ctrl+S")
		save_action.setStatusTip("Save edits made in table to database")
		# save.triggered.connect(self.save)

		exit_action = QtWidgets.QAction(QtGui.QIcon("exit.png"), "&Quit JAVBrowse", self)
		exit_action.setShortcut("Ctrl+Q")
		exit_action.setStatusTip("Quit JAVBrowse")
		exit_action.triggered.connect(self.quit)

		reset_action = QtWidgets.QAction(QtGui.QIcon(""), "&Reset Database", self)
		reset_action.setStatusTip("Reset the JAVBrowse database")
		reset_action.triggered.connect(self.reset)

		file_menu = menubar.addMenu("&File")
		file_menu.addAction(load_folder)
		file_menu.addAction(save_action)
		file_menu.addAction(exit_action)
		file_menu.addAction(reset_action)

		# Other menu
		statistics_action = QtWidgets.QAction(QtGui.QIcon(""), "Statistics", self)
		statistics_action.setStatusTip("Statistics of current database")
		statistics_action.triggered.connect(self.view_statistics)

		other_menu = menubar.addMenu("&Other")
		other_menu.addAction(statistics_action)




	def initUI(self):
		self.init_menus()
		self.init_window()

	def init_window(self):

		self.table_display = TableDisplay(self)
		self.table_display.control_widget.search_field.textChanged.connect(self.search)
		self.setCentralWidget(self.table_display)

		self.debug_widget = QtWidgets.QTextEdit()
		self.debug_widget.show()

		self.status_label = QLabel()
		self.statusBar().addWidget(self.status_label)


		self.showMaximized()
		

	def init_triggers(self):
		self.update_statusbar_trigger.connect(self.update_status_bar)

	def load_folder(self):
		def helper():
			self.update_statusbar_trigger.emit("Loading directory")
			if not dir_path:
				self.update_statusbar_trigger.emit("Canceled")
				return
			videos = core.get_video_paths(dir_path)
			self.update_statusbar_trigger.emit("{} Videos found in directory".format(len(videos)))
			linked, unlinked = core.link_code_to_videos(videos)
			self.update_statusbar_trigger.emit("Video codes linked to {} videos, failed to link for {} videos".format(len(linked), len(unlinked)))
			for proc, total, failed, multi in self.model.tag_linked_videos(linked):
				self.update_statusbar_trigger.emit("{}/{} total Videos processed ({} failed, {} multi)".format(proc, total, failed, multi))
			return 0
		self.update_status_bar("test")
		dir_path = QtWidgets.QFileDialog.getExistingDirectory(self, "Load folder into JAVB", )
		threading.Thread(target = helper).start()
		self.refresh()
		return 0

	def update_status_bar(self, string):
		# self.statusBar().showMessage(string)
		self.status_label.setText(string)
		return 0

	def view_statistics(self):
		print("VIEWING STATSs")
		message_box = QMessageBox()
		message_box.setText("<b>JAVB statistics</b>")
		statistics = self.model.get_statistics()

		message_box.setInformativeText("Total Videos Stored: {}\n".format(statistics["video_count"]) + 
									   "")
		message_box.exec_()


	def search(self):
		"""
		Takes input from control box of table widget and searches the tag_database
		of the core system	
		"""
		string = self.table_display.get_search_text()
		entries = self.model.search(string, and_flag = self.table_display.get_search_status())
		self.table_display.update_table(entries)

	def quit(self):
		self.debug_widget.quit()
		


	def debug(self, *args):
		temp = " ".join([str(i) for i in args])
		self.debug_widget.append(temp)

	def refresh(self):
		self.table_display.init_table()
		self.table_display.control_widget.create_and_update_completer(self.model.get_all_tags())

	def reset(self):
		self.model.reset_database()
		self.refresh()


	def closeEvent(self, event):
		self.debug_widget.close()
		super().closeEvent(event)

class ControlBox(QWidget):
	def __init__(self):
		super().__init__()

		self.init_UI()

	def init_UI(self):
		hbox = QtWidgets.QHBoxLayout()

		self.push = QtWidgets.QPushButton("Search")
		self.search_field = QtWidgets.QLineEdit()
		label = QtWidgets.QLabel("Search", self.search_field)
		
		# handle the autocompleter
		self.create_and_update_completer()
		hbox.addWidget(self.push	)
		hbox.addWidget(label)
		hbox.addWidget(self.search_field)

		self.dt1 = QtWidgets.QRadioButton("OR")
		self.dt1.setChecked(True)
		self.dt2 = QtWidgets.QRadioButton("AND")
		hbox.addWidget(self.dt1)
		hbox.addWidget(self.dt2)

		self.setLayout(hbox)

	def create_and_update_completer(self, wordlist = []):
		self.completer = Completer(wordlist)
		self.search_field.setCompleter(self.completer)

class TableDisplay(QWidget):
	def __init__(self, parent):
		super().__init__()
		self.parent = parent
		self.lang = parent.lang
		self.init_UI()
		

	def init_UI(self):
		self.control_widget = ControlBox()
		self.control_widget.create_and_update_completer(self.parent.model.get_all_tags())
		self.init_table()
		self.init_layout()

	def init_table(self):
		header_labels = [
		"Code", 	# 0
		"Title", 	# 1
		"Maker",	# 2 
		"Director",	# 3
		"Label", 	# 4 
		"Stars", 	# 5
		"Tags",   	# 6
		"Release Date",  # 7
		"_path", 	# 8
		"Rating",   # 9
		]
		self.table_widget = QtWidgets.QTableWidget()
		self.table_widget.setSortingEnabled(True)
		self.table_widget.setRowCount(0)
		self.table_widget.setColumnCount(9)
		self.table_widget.setHorizontalHeaderLabels(header_labels)
		self.table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		# Hide the ID, path
		# self.table_widget.setColumnHidden(7, 1)
		# self.table_widget.setColumnHidden(8, 1)
		
		# Set to english
		self.toggle_language()
		#set click behavior
		self.table_widget.cellDoubleClicked.connect(self.open)
		self.table_widget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

	def open(self, row, col):
		path = self.table_widget.item(row, 8).text()
		print(os.path.exists(path))
		if sys.platform.startswith("linux"):
			subprocess.call(["xdg-open", path])
		else:
			try:
				print("opening {}".format(path))
				os.startfile(path)
			except:
				msg = QtWidgets.QMessageBox()
				msg.setIcon(QtWidgets.QMessageBox.Information)
				msg.setInformativeText("No Such file Exists")
				msg.setWindowTitle("Error")
				msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
				msg.exec_()


	
	def toggle_language(self):
		return
		#BUGGED. FIX THIS
		eng = list(range(1,6))
		jp = list(range(6, 11))
		#switch from english to japanese
		if self.lang == 0:
			for i in eng:
				self.table_widget.setColumnHidden(i, 1)
			for i in jp:
				self.table_widget.setColumnHidden(i, 0)
		# Back to eng
		else:
			for i in eng:
				self.table_widget.setColumnHidden(i, 0)
			for i in jp:
				self.table_widget.setColumnHidden(i, 1)
		self.lang = not self.lang

	def init_layout(self):
		grid = QtWidgets.QGridLayout()
		grid.setSpacing(10)
		grid.addWidget(self.control_widget, 1, 0)
		grid.addWidget(self.table_widget, 2, 0)
		self.setLayout(grid)

	# Controlbox management

	def get_search_status(self):
		if self.control_widget.dt1.isChecked():
			return "OR"
		else:
			return "AND"

	def get_search_text(self):
		return self.control_widget.search_field.text()

	def update_table(self, entries):
		self.table_widget.clearContents()
		self.table_widget.setRowCount(0)
		# Populate rows
		for row, entry in enumerate(entries):
			self.table_widget.insertRow(row)
			print(entry)
			code = entry["code"]
			title = entry[self.lang]["title"]
			star = entry[self.lang]["star"]
			star = ", ".join([i for i in star])
			genre = entry[self.lang]["genre"]
			genre = ", ".join([i for i in genre])
			maker = entry[self.lang]["maker"]
			director = entry[self.lang]["director"]
			label = entry[self.lang]["label"]
			release_date = entry[self.lang]["release_date"]
			path = entry["path"]
			for num, entry in enumerate([code, title, maker, director, label, star, genre, release_date, path ]):
				self.table_widget.setItem(row, num, QtWidgets.QTableWidgetItem(entry))
		


def run():
	app = QApplication(sys.argv)
	print("starting app")
	ex = Main()
	sys.exit(app.exec_())


if __name__ == "__main__":

	app = QApplication(sys.argv)
	print("starting app")
	ex = Main(debug = 1)
	print(ex.model.database)

	sys.exit(app.exec_())
