import re
import requests
import constants
import logging
import errors

class Searcher:
    """
    Core searcher class
    """
    def __init__(self, image_dir = None):
        self.searchers = [JavlibrarySearcher()]
        self.image_dir = image_dir



    def get_details(self, code, get_images = 0):
        """
        """
        return self.searchers[0].get_details(code, get_images = get_images)

    def __bool__(self):
        return False



class CompiledReDummy:
    def __init(self):
        pass
    def findall(self, string):
        return []

class SiteSearcher:
    """
    SiteSearcher
    Core object which searches a specfic site for information on a specific video
    """
    def __init__(self, search_path, fail_path, access_path, langs = []):
        """
        arguments
        search_path: <str> path to search. String must contain two indexed format {n} to allow input of language and code
            {0}: language, {1}: code
        """
        self.search_path = search_path
        self.fail_path = fail_path
        #Direct access 
        self.access_path = access_path
        self.langs = langs


        #Regexes
        self.star_re = CompiledReDummy()         # Multiple
        self.genre_re = CompiledReDummy()        # Multiple
        self.director_re = CompiledReDummy()     # Single
        self.maker_re = CompiledReDummy()        # Single
        self.label_re = CompiledReDummy()        # Single
        self.title_re = CompiledReDummy()        # Single
        self.release_date_re = CompiledReDummy() # Single
        self.image_re = CompiledReDummy()        # Single
        self.multi_results_re = CompiledReDummy()        # Single

    def get_first(self, items):
        if not items:
            return ""
        else:
            return items[0]

    def get_details(self, code, get_images = 0, debug = 0):
        """
        get_details
        acquires japanese and english details for a given JAV code

        returns:
        tuple (en, jp), where en and jp are dictionaries containing their respective items
        """
        res = []
        for lang in self.langs:
            # print(lang)
            site = self.search_path.format(lang, code)
            req = requests.get(site)
            text = req.text
            # Check if the search failed (No or Multiple)
            if req.url.startswith(self.fail_path.format(lang)):
                # multi re returns a list of (path, code) tuples
                multi = self.multi_results_re.findall(text)
                # Check if the database contains multiple results
                if multi:
                    # Try to guess which one is the code we want (exact match)
                    # try an exact match
                    options = [item for item in multi if item[1] == code]
                    if len(options) == 1:
                        req = requests.get(self.access_path.format(lang, options[0][0]))
                        print(req.url)
                        text = req.text
                    else:
                        raise errors.SearchMultipleResults
                else:
                # Insert some sort of no or multi result checker here
                    logging.info("No or many results found for code: " + code)
                    raise errors.SearchNoResults

            

            title = self.get_first(self.title_re.findall(text))
            star  = self.star_re.findall(text)
            genre = self.genre_re.findall(text)
            maker = self.get_first(self.maker_re.findall(text))
            label = self.get_first(self.label_re.findall(text))
            director = self.get_first(self.director_re.findall(text))
            release_date = self.get_first(self.release_date_re.findall(text))

            res.append({
            "code": code,   #single
            "title": title, #single
            "star": star,   #many
            "genre": genre, #many
            "maker": maker, #single
            "label": label, #single
            "director": director,
            "release_date": release_date
            })
            if all(not res[0][i] for i in res[0] if res[0][i] != "code"):
                print("No results found")
                raise errors.FailedToAcquireTags

        return res

class JavlibrarySearcher(SiteSearcher):
    def __init__(self):
        super().__init__('http://www.javlibrary.com/{0}/vl_searchbyid.php?keyword={1}', 
                         "http://www.javlibrary.com/{0}/vl_search", 
                         "http://www.javlibrary.com/{0}{1}",
                         ["en", "ja"])
        #Regexes
        self.star_re = re.compile(r'vl\_star\.php\?s\=[a-z0-9]{1,10}" rel="tag">(.{1,20})</a>')
        self.genre_re = re.compile(r'vl\_genre\.php\?g=[a-z0-9]+" rel="category tag">(.{1,20})</a>')
        self.director_re = re.compile(r'vl\_director\.php\?d=[a-z0-9]+" rel="tag">(.{1,20})</a>')
        self.maker_re = re.compile(r'vl\_maker\.php\?m=[a-z0-9]+"\srel="tag">(.{1,20})</a>')
        self.label_re = re.compile(r'vl\_label\.php\?l=[a-z0-9]+"\srel="tag">(.{1,20})</a>')
        self.title_re = re.compile(r'<meta property="og:title" content=".*? (.*) - JAVLibrary" \/>')
        self.release_date_re = re.compile(r'video_date.*\n.*\n.*\n.*\n.*"text">(.*)<\/td>', re.MULTILINE)
        self.image_re = re.compile(r'video_jacket_img" src="(.*\.jpg)" ')
        self.multi_results_re = re.compile(r"""f="\.(.*)" title*.*<div class="id">([\w-]*)<""")




if __name__ == "__main__":
    # test = requests.get
    s = Searcher()
    print("Searching HODV-21196")
    print(s.get_details("HODV-21196"))
    print("Searching SOAN-001")
    print(s.get_details("SOAN-001"))