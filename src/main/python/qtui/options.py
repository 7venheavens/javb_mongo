from PyQt5 import QtWidgets
from PyQt5 import Qt
import os, sys


class OptionsMenu(QtWidgets.QWidget):
	def __init__(self,  *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.options = ["Storage"]
		self.option_map = {"Storage": self.init_storage_view}

		self.init_UI()
		self.init_bindings()
		
	def init_parent(self):
		pass

	def init_UI(self):
		self.setWindowTitle("Options")
		self.init_base_view()
		self.show()

	def init_base_view(self):
		"""
		Generate a 2:3 distribution. Left is a listwidget, right a scrollarea
		"""
		layout = QtWidgets.QHBoxLayout(self)

		self.select = QtWidgets.QListWidget(self)
		self.select.addItems(self.options)
		ssp = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
		ssp.setHorizontalStretch(1)
		self.select.setSizePolicy(ssp)
		self.scrollbox = QtWidgets.QScrollArea(self)
		sbsp = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
		sbsp.setHorizontalStretch(3)
		self.scrollbox.setSizePolicy(sbsp)
		layout.addWidget(self.select)
		layout.addWidget(self.scrollbox)

	def init_bindings(self):
		self.select.itemClicked.connect(self.swap_view)
	def test(self, *args):
		for i in args:
			print(i.text())

	def init_storage_view(self):
		self.clean_up()
		layout, select, scroll = self.init_base_view()
		self.setLayout(layout)

	def swap_view(self, qlistitem):
		self.option_map[qlistitem.text()]()
	def clean_up(self):
		try:
			self.layout
		except:
			print("No layout")
		Qt.QObjectCleanupHandler().add(self.layout)
		


class Tester(QtWidgets.QWidget):
	def __init__(self):
		super().__init__()
		self.a = 1
		self.database = "JAVB_TEST"


if __name__ == "__main__":

	app = QtWidgets.QApplication(sys.argv)
	options = OptionsMenu()
	sys.exit(app.exec_())
