from bisect import insort
from sortedcontainers import SortedListWithKey

class Node:
	def __init__(self):
		self.targets = dict()
		self.contents = SortedListWithKey([], key = lambda x: x[0])
		self.sorted_keys = []

	def __repr__(self):
		return "<Node object | targets: {} | contents: {}>".format([i for i in self.targets], [i for i in self.contents])

	def get_or_create(self, key):
		"""
		Takes a letter key. If target is non present, creats and returns a new node in targets, 
		otherwise, simply returns the node

		# Note: binary search replaced with linear temporarily
		"""
		try:
			return self.targets[key]
		except:
			self.targets[key] = Node()
			insort(self.sorted_keys, key)
			return self.targets[key]

	def get_all(self):
		"""
		returns a sorted list of all words contained by this node and it's descendents
		(maybe limit it)
		"""
		res = [i[1] for i in self.contents]
		for key in self.sorted_keys:
			res.extend(self.targets[key].get_all())
		return res

	def add(self, key, word):
		"""
		NOTE: add is not unique. Can repeatedly add the same entry
		"""
		self.contents.add((key, word))


class Trie:
	"""
	"""
	def __init__(self):
		self.top = Node()

	def add_one(self, key, item = None):
		"""
		Adds an item to the trie using an iterable key
		"abc" -> a -> b -> c
		["ab", c] -> ab -> c
		arguments:
			key   : iterable key to store item
			item  : contents to be stored
		"""
		# print("adding {}, {}".format(key, item))
		if not key:
			raise
		if not item:
			item = key
		cur_node = self.top
		for index in key[:-1]:
			cur_node = cur_node.get_or_create(index)

		fin_index = key[-1]
		fin_node = cur_node.get_or_create(fin_index)

		fin_node.add(fin_index, item)

	def add_many(self, keys, items = None):
		if not keys:
			return
		if not items:
			items = keys
		for key, item in zip(keys, items):
			self.add_one(key, item)
			

	def delete_one(self, word):
		pass

	def search(self, key):
		"""
		Searches the Trie for a given word or all words starting with word
		"""
		cur_node = self.top
		for index in key:
			if index not in cur_node.targets:
				# print("{} not in current node".format(index))
				return []
			else:
				cur_node = cur_node.targets[index]

		return cur_node.get_all()


if __name__ == "__main__":
	# Basic functionality tests
	trie = Trie()
	trie.add_one("ab")
	trie.add_one("abc")
	trie.add_one("abde")
	trie.add_one("abfg")
	print(trie.search("a"))
	print(trie.top.targets["a"].targets["b"])
	print(trie.search("a"))
	print(trie.top.targets["a"].targets["b"].targets["f"].targets["g"])
	# print(trie.top.targets["a"].targets)
	assert trie.search("a") == ["ab", 
								"abc", 
								"abde", 
								"abfg"]
	trie.add_one("abcde")
	trie.add_one("abcde")
	trie.add_one("abcdefg")
	assert trie.search("abc") == [
									"abc", 
									"abcde", 
									"abcde", 
									"abcdefg"]								
	# Performance test
	from random import SystemRandom
	import string
	from timeit import timeit
	print("generating strings")
	strings = ["".join(SystemRandom().choice(string.ascii_lowercase) for i in range(SystemRandom().randint(2, 12))) for i in range(10000)]
	# print("timing addition of 10000 strings to trie" )
	# print(timeit(lambda: trie.add_many(strings), number = 100))
	print("timing random 3 letter string searches")
	trie_2 = Trie()
	trie_2.add_many(strings)
	search_strings = ["".join(SystemRandom().choice(string.ascii_lowercase) for i in range(3)) for i in range(10000)]
	print(timeit(lambda: [trie_2.search(i) for i in search_strings], number = 1))
	print(trie_2.search("ab"))

	trie_3 = Trie()
	trie_3.add_many(["a", "abc", "bc"], [123, 12, 1514])
	print(trie_3.search("a"))
	print(trie_3.search("b"))



	


